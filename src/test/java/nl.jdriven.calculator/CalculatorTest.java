package nl.jdriven.calculator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNotNull;

public class CalculatorTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    Calculator calculator = new Calculator();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

    /* Start here, move this line one case at a time

    //Create a calculator class in the package nl.jdriven.calculator
    @Test
    public void testConstructionOfCalculator() {
        assertNotNull(calculator);
        assertTrue(calculator instanceof Calculator);
    }

    // Add
    @Test
    public void testCalculatorAdd() {
        assertEquals(5, calculator.add(3, 2));
    }

    //Minus
    @Test
    public void testCalculatorSubtract() {
        assertEquals(1, calculator.subtract(3, 2));
    }

    //Multiply
    @Test
    public void testCalculatorMultiply() {
        assertEquals(6, calculator.multiply(3, 2));
    }

    //Divide
    @Test
    public void testCalculatorDivide() {
        assertEquals(1.5, calculator.divide(3, 2));
    }


    //Is it Larger? yes / no // use if else
    @Test
    public void testIsLargerThen() {
        assertTrue(calculator.isLargerThen(3, 2));
    }

    //String based cals
    //doThis("add", 1, 2)
    //doThis("minus, 1,2)
    //doThis("multiply, 2,2)
    //doThis("asdasda") throw IllegalArgumentException
    @Test
    public void testStringBasedAdd() {
        assertEquals(5, calculator.doThis("add", 3, 2));
    }

    @Test
    public void testStringBasedSubtract() {
        assertEquals(1, calculator.doThis("subtract", 3, 2));
    }

    @Test
    public void testStringBasedMultiply() {
        assertEquals(6, calculator.doThis("multiply", 3, 2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStringBasedException() {
        calculator.doThis("asdafasda", 3, 2);
    }

    // Print all values from 1 to 10
    // print(1,10)
    @Test
    public void testPrint() {
        String expectation = "1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n";
        calculator.print(1, 10);
        assertEquals(expectation, outContent.toString());
    }

    // Print all values with max lines using break print(1,1000000, 20)
    @Test
    public void testPrintWithMax() {
        String expectation = "1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n";
        calculator.print(1, 1000000, 20);
        assertEquals(expectation, outContent.toString());
    }

    */
}